# Twitter_Feed

This is a pseudo-backport of the Drupal 8 [Twitter_Feed][d8_twitter_feed] module.

A theme should be able to override the template files `twitter-tweet.tpl.php` and `twitter-feed.tpl.php` as normal.

[d8_twitter_feed]: https://www.drupal.org/project/issues/twitter_feed
